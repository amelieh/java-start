package com.codelutin;
import java.util.Scanner;

public class ChristmasDay {
    public static void main(String[]args) {
        int annee;
        Scanner keyboardInput = new Scanner(System.in);

        System.out.println("Saisir une année entre 2000 et 2099 :");
        annee = keyboardInput.nextInt();

        int tempspasse; /* le temps passé */
        int xbis; /* le nombre d'année bissextile */
        int xjour; /* le nombre de jour à ajouter par rapport à la référence soit lundi */

        String joursemaine[] = new String[]{"Lundi ", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"};

        tempspasse = annee - 2000;
       /* System.out.println("Il y a " + tempspasse + "  ans, Noël a eu lieu un lundi"); */

        xbis = tempspasse / 4;
        /* System.out.println("Le nombre d'années bissextiles dans cette période est de : " + xbis); */

        xjour = (tempspasse + xbis);
        /*System.out.println("il faut ajouter " + xjour + " jours au lundi "); */

        if (xjour % 7 == 0) {
            System.out.println("Cette année là, Noël tombe un : " + joursemaine[0]);
        } else if (xjour % 7 == 1) {
            System.out.println("Cette année là, Noël tombe un : " + joursemaine[1]);
        } else if (xjour % 7 == 2) {
            System.out.println("Cette année là, Noël tombe un : " + joursemaine[2]);
        } else if (xjour % 7 == 3) {
            System.out.println("Cette année là, Noël tombe un : " + joursemaine[3]);
        } else if (xjour % 7 == 4) {
            System.out.println("Cette année là, Noël tombe un : " + joursemaine[4]);
        } else if (xjour % 7 == 5) {
            System.out.println("Cette année là, Noël tombe un : " + joursemaine[5]);
        } else {
            System.out.println("Cette année là, Noël tombe un : " + joursemaine[6]);
        }

    }
}
